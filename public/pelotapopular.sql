-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-11-2014 a las 20:36:05
-- Versión del servidor: 5.5.33
-- Versión de PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `pelotapopular`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE IF NOT EXISTS `equipos` (
  `equipo_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`equipo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`equipo_id`, `nombre`) VALUES
(1, 'Aguilas Cibaeñas'),
(2, 'Gigantes del Cibao'),
(3, 'Estrellas Orientales'),
(4, 'Toros del Este'),
(5, 'Escogido'),
(6, 'Tigres del Licey');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_bateo`
--

CREATE TABLE IF NOT EXISTS `historial_bateo` (
  `historial_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `bateo` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`historial_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `historial_bateo`
--

INSERT INTO `historial_bateo` (`historial_id`, `user_id`, `bateo`, `created_date`) VALUES
(1, 1, 5, '2014-11-12 00:00:00'),
(2, 2, 10, '2014-11-12 00:00:00'),
(3, 3, 2, '2014-11-12 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincias`
--

CREATE TABLE IF NOT EXISTS `provincias` (
  `id_provincia` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id_provincia`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=30 ;

--
-- Volcado de datos para la tabla `provincias`
--

INSERT INTO `provincias` (`id_provincia`, `name`) VALUES
(1, 'Azua'),
(2, 'Baoruco'),
(3, 'Barahona'),
(4, 'Dajabón'),
(5, 'El Seibo'),
(6, 'Elías Piña'),
(7, 'Espaillat'),
(8, 'Hato Mayor'),
(9, 'Hermanas Mirabal'),
(10, 'Independencia'),
(11, 'La Altagracia'),
(12, 'La Romana'),
(13, 'La Vega'),
(14, 'María Trinidad Sánchez'),
(15, 'Monseñor Nouel'),
(16, 'Montecristi'),
(17, 'Pedernales'),
(18, 'Peravia'),
(19, 'Puerto Plata'),
(20, 'Samaná'),
(21, 'San Cristóbal'),
(22, 'San José de Ocoa'),
(23, 'San Juan'),
(24, 'San Pedro de Macorís'),
(25, 'Sánchez Ramírez'),
(26, 'Santiago'),
(27, 'Santiago Rodríguez'),
(28, 'Santo Domingo'),
(29, 'Valverde');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8_bin NOT NULL,
  `cedula` varchar(25) COLLATE utf8_bin NOT NULL,
  `id_provincia` int(11) NOT NULL,
  `equipo_id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `telefono` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(100) COLLATE utf8_bin NOT NULL,
  `created_date` date NOT NULL,
  `is_active` int(11) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `nombre`, `cedula`, `id_provincia`, `equipo_id`, `email`, `telefono`, `password`, `created_date`, `is_active`) VALUES
(1, 'Daniel A Sanchez De Oleo', '555', 1, 5, 'dasdo1@gmail.com', '3058887271', '$2a$08$dpw0YD6.uAzPsjkhVpf8IeQkG8JKgA4fHe1AR0ID.Nc473zQ6GXCy', '2014-11-06', 1),
(2, 'Maximo castro', '55566', 2, 6, 'max@mctekk.com', '3058887271', '$2a$08$h94hOY/kDwuDNaRvHRQcTeg9pq/TTNiEZE5cAQm6fCcR5qce5ckp2', '2014-11-06', 1),
(3, 'Douglas', '55566', 15, 4, 'lmclqjak@sharklasers.com', '3058887271', '$2a$08$kUjAG27XUodUbd4cTi5DJOFTfMozcDGGKWaAXOus2p7m8YGZ1dpDi', '2014-11-06', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
