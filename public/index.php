<?php
header('X-Frame-Options: DENY'); 
header('X-XSS-Protection: 1; mode=block'); 
header('X-Content-Type-Options: nosniff'); 

ini_set('session.cookie_httponly', true);
ini_set('session.cookie_secure', isset($_SERVER['HTTPS']));
use Phalcon\Http\Response;

//error_reporting(0);
 // Session security flags
    ini_set('session.cookie_httponly', 1);
    ini_set('session.use_only_cookies', 1);
    ini_set('session.cookie_secure', 1);

try {

	/**
	 * Read the configuration
	 */
	$config = include __DIR__ . "/../app/config/config.php";

	/**
	 * Read auto-loader
	 */
	include __DIR__ . "/../app/config/loader.php";

	/**
	 * Read services
	 */
	include __DIR__ . "/../app/config/services.php";

	/**
	 * Handle the request
	 */
	$application = new \Phalcon\Mvc\Application($di);
	
	echo $application->handle()->getContent();

} catch (\Exception $e) {
//	echo $e->getMessage();
	 /**
             * Show an static error page
             */
            $response = new Response();
            $response->redirect('/');
            $response->send();

}
