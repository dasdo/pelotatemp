<?php

use Phalcon\DI\FactoryDefault,
	Phalcon\Mvc\View,
	Phalcon\Mvc\Url as UrlResolver,
	Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
	Phalcon\Mvc\View\Engine\Volt as VoltEngine,
	Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter,
	Phalcon\Session\Adapter\Files as SessionAdapter,
	Phalcon\Mvc\Dispatcher as PhDispatcher;



/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function() use ($config) {
	$url = new UrlResolver();
	$url->setBaseUri($config->application->baseUri);
	return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function() use ($config) {

	$view = new View();

	$view->setViewsDir($config->application->viewsDir);

	$view->registerEngines(array(
		'.volt' => function($view, $di) use ($config) {

			$volt = new VoltEngine($view, $di);

			$volt->setOptions(array(
				'compiledPath' => $config->application->cacheDir,
				'compiledSeparator' => '_'
			));

			return $volt;
		},
		'.phtml' => 'Phalcon\Mvc\View\Engine\Php'
	));

	return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function() use ($config) {
	return new DbAdapter(array(
		'host' => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname' => $config->database->dbname,
		'charset' => 'utf8'
	));
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function() {
	return new MetaDataAdapter();
});

$di->set(
    'dispatcher',
    function() use ($di) {

        $evManager = $di->getShared('eventsManager');

        $evManager->attach(
            "dispatch:beforeException",
            function($event, $dispatcher, $exception)
            {
                switch ($exception->getCode()) {
                    case PhDispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                    case PhDispatcher::EXCEPTION_ACTION_NOT_FOUND:
                        $dispatcher->forward(
                            array(
                                'controller' => 'index',
                                'action'     => 'index',
                            )
                        );
                        return false;
                }
            }
        );
        $dispatcher = new PhDispatcher();
        $dispatcher->setEventsManager($evManager);
        return $dispatcher;
    },
    true
);

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function() {
        $config = [
        'cookie.name'     => 'sid',
        'cookie.lifetime' => 345600,
        'cookie.path'     => "/",
        'cookie.domain'   => ".emocionpopular.com.do",
        'cookie.secure'   => 1,
        'cookie.httponly' => 1
        ];  
	     $session = new \Pelota\Session($config); 
        $session->start();
        return $session;
});

//Register the flash service with custom CSS classes
$di->set('flash', function(){
   $flash = new \Phalcon\Flash\Direct(array(
       'error' => 'alert alert-danger',
       'success' => 'alert alert-success',
       'notice' => 'alert alert-info',
   ));
   return $flash;
});

$di->set('facebook', function() use ($config){

   $facebook = new Facebook(
   array(

	   'appId' => '688310981285233',
	   'secret' => 'f18b3a94d2f136db5d937f8a13d70bcc'

	  ));

	$scope = 'user_status,email,publish_actions,offline_access,read_friendlists,status_update,user_birthday';

	$fb = new FacebookLibrary($facebook,$scope);
	return $fb;
});

/**
* Config the default cache storage
*/
$di->set('cache', function() use ($config) {

    //Create a Data frontend and set a default lifetime to 1 hour
    $frontend = new Phalcon\Cache\Frontend\Data(array(
        'lifetime' => 3600
    ));

    // Set up Memcached and use tracking to be able to clean it later.
    // You should not use tracking if you're going to store a lot of keys!
    $cache = new Phalcon\Cache\Backend\Memcache($frontend, array(
        "host" => $config->memcache->host,
        "port" => $config->memcache->port,
    ));

    return $cache;
});

//Mail service uses Gmail;
$di->set('mail', function(){
		return new Mail();
});