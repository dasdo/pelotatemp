<?php

$loader = new \Phalcon\Loader();

$loader->registerNamespaces(array(
    'Pelota' => $config->application->libraryDir,
));

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
	array(
		$config->application->controllersDir,
		$config->application->modelsDir,
		$config->application->libraryDir
	)
);

$loader->registerClasses(
 array(
 "FacebookLibrary"   => '/home/dasdo/pelota/app/library/Mars/Social/FacebookLibrary.php',
 "Facebook" => '/home/dasdo/pelota/app/library/Mars/Social/sdk/facebook.php',
 "Twitter" => '/home/dasdo/pelota/vendor/dg/twitter-php/src/twitter.class.php'
 ));
 $loader->register();
