<?php

class AdminController extends ControllerBase
{
	public function onConstruct(){
		if (!$this->session->has("userData") || $this->session->get("userData")->role != 1)
           	{
			return $this->response->redirect();
		}
	}
    public function indexAction()
    {
		$this->view->page = $this->request->get('page','int') > 1 ? $this->request->get('page','int') : 1;
		$page = $this->request->get('page','int') > 1 ? ($this->request->get('page','int') -1) * 10 : 0;
		$nextPage = $this->request->get('page','int') == 0 ? 2 : $this->request->get('page','int') + 1;
		$prevPage = $this->request->get('page','int') > 0 &&  $this->request->get('page','int') != 1 ? $this->request->get('page','int') - 1 : null;

		$this->view->users = Users::find(['limit' => ['number' => 10, 'offset' => $page], "order" => "total_bateo desc"]);
		$this->view->count = Users::count();
		$search = strtolower($this->request->getPost("search", "string"));
		
		$linkLeft = is_null($prevPage) ? '#' : '/admin?page='.$prevPage.'#page';
		$linkRight = is_null($nextPage) ? '#' : '/admin?page='.$nextPage.'#page';

		if($search)
		{
			//LOWER(`Value`)
			$users = Users::find([
				'conditions' => "LOWER(email) like ?1 OR LOWER(nombre) like ?2",
				'bind' => [1 => "%{$search}%", 2 => "%{$search}%"]
			]);

			$this->view->users = $users;
		}
        $this->view->setVar('linkLeft', $linkLeft);
        $this->view->setVar('linkRight', $linkRight);
    }
	
	public function convertcsvAction()
	{
		$users = Users::find();
		$file = 'csv/user.'.time().'.txt';
		$path = str_replace("/app/controllers","",__DIR__) . '/public/'.$file;
		$csvFile = new Keboola\Csv\CsvFile($path);
		$csvFile->writeRow(["ID","Nombre","Telefono","Cedula","Email","Provincia","Equipo","Total de bateo"]);
		foreach($users as $user)
		{
			$total = (int) $user->total_bateo;
			$csvFile->writeRow([$user->user_id, $user->nombre, $user->telefono, $user->cedula,$user->email, $user->provincias->name, $user->equipos->nombre, $total]);
		}
		$url = "http://".$_SERVER['SERVER_NAME']."/".$file;
		return $this->response->redirect($url);
	}

}

