<?php

use Phalcon\Mvc\Model\Criteria,
    Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\View;

class RegistroController extends ControllerBase
{

    /**
     * Index action
     */
    public function indexAction()
    {
         $this->view->provincia = Provincias::find();
         $this->view->equipos = Equipos::find();
		$this->view->facebook = $this->session->get("facebook");
    }

    public function bateoAction()
    {
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);

    	//guardamos el bateo

    	if($this->request->get("user_id", 'int'))
    	{
    	   $historial = new HistorialBateo();
    	   $historial->user_id = $this->request->get("user_id", 'int');
    	   $historial->bateo = $this->request->get("bateo", 'int');
    	   $historial->created_date = date('Y-m-d H:i:s');
    	   $historial->save();

    	   $totalBateos = $historial->sum(['column' => 'bateo', 'conditions' => "user_id = '{$this->request->get("user_id", 'int')}'"]);
    	   $user = Users::findFirstByUser_id($historial->user_id);
    	   $user->total_bateo = $totalBateos;
    	   $user->update();
    	   return ;
    	}
    }
	
    /**
     * Creates a new user
     */
    public function createAction()
    {
        if (!$this->request->isPost() && !$this->security->checkToken()) {
            return $this->dispatcher->forward(array(
                "controller" => "registro",
                "action" => "index"
            ));
        } 

		if ($this->request->getPost("equipo_id", "string") == '-1')
		{
			$this->flash->error("Selecciona tu equipo");
			 return $this->dispatcher->forward(array(
                "controller" => "registro",
                "action" => "index"
            ));

		}
		
		if ($this->request->getPost("password") != $this->request->getPost("password2")) {
			$this->flash->error("Las contraseñas presentan problemas.");
            return $this->dispatcher->forward(array(
                "controller" => "registro",
                "action" => "index"
            ));
        }
		
		$cedula = filter_var(str_replace(["-","+"],"",$this->request->getPost("cedula")),FILTER_SANITIZE_NUMBER_INT);
		if (strlen($cedula) != 11) {
			$this->flash->error("Introduce un número de cédula válido");
            return $this->dispatcher->forward(array(
                "controller" => "registro",
                "action" => "index"
            ));
        }
		
		$telefono = filter_var(str_replace(["-","+"], "",$this->request->getPost("telefono")),FILTER_SANITIZE_NUMBER_INT);
		if (strlen($telefono) > 11 && strlen($telefono) < 10) {
			$this->flash->error("Introduce un teléfono válido");
            return $this->dispatcher->forward(array(
                "controller" => "registro",
                "action" => "index"
            ));
        }
		
        $user = new Users();

        $user->nombre = $this->request->getPost("nombre", 'string').' '.$this->request->getPost("apellido", 'string');
        $user->cedula = $cedula;
        $user->id_provincia = $this->request->getPost("id_provincia",'int');
        $user->equipo_id = $this->request->getPost("equipo_id", 'int');
        $user->email = $this->request->getPost("email", "email");
        $user->telefono = $this->formatPhoneNumber($telefono);
        $user->password = $this->security->hash($this->request->getPost("password", "string"));
        $user->created_date = date("Y-m-d");
        $user->is_active = 1;
        
		$checkemail = Users::findFirst([
			"email = :email:",
			"bind" => [
				"email" => $user->email
		]]);
		
		$checkid = Users::findFirst([
			"cedula = :cedula:",
			"bind" => [
				"cedula" => $user->cedula
		]]);
		if($checkemail || $checkid){
			$this->flash->error("Ups! Parece que estas registrado, prueba con otro email o inicia sesión <a href='/login'>aquí</a>.");
            return $this->dispatcher->forward(array(
                "controller" => "registro",
                "action" => "index"
            ));
		}

        if (!$user->save()) {
            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->dispatcher->forward(array(
                "controller" => "registro",
                "action" => "index"
            ));
        }

        $this->flash->success("user was created successfully");
        return $this->dispatcher->forward(array(
            "controller" => "login",
            "action" => "index"
        ));

    }

    public function recuperadoAction()
    {
                $this->view->disableLevel(View::LEVEL_MAIN_LAYOUT);

    }

}
