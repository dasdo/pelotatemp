<?php



use Phalcon\Mvc\Model\Criteria,

    Phalcon\Paginator\Adapter\Model as Paginator,

	Phalcon\Validation\Validator\PresenceOf,

	Phalcon\Validation;



class LoginController extends ControllerBase

{

	 /**

     * Index action

     */

    public function indexAction()

    {

		if ($this->session->has("userData")) {

           return $this->response->redirect('http://emocionpopular.com.do/conecta');

        } 

		

		if (!$this->request->isPost() )

			return ;

			

		if (!$this->request->isPost() && !$this->security->checkToken()) {

            return;

        } 



		if($this->request->getPost('new')) 

			return $this->response->redirect('http://emocionpopular.com.do/registro');



		$username = $this->request->getPost('email');

		$password = $this->request->getPost('password');

		$remember = 1;



		//Ok let validate user password

		$validation = new Validation();

		$validation->add('email', new PresenceOf(['message' => 'No debes dejar el email vacio']));

		$validation->add('password', new PresenceOf(['message' => 'La contraseña no puede estar en blanco']));



		//validate this form for password

		$messages = $validation->validate($_POST);

		if (count($messages))

		{

			foreach ($messages as $message)

			{

				$this->flash->error($message);

			}

			return;

		}

		

		 //login the user

		try

		{

			$users = new Users();

			$userData =  $users->login($username, $password, $remember);

			$this->session->set("userData", $userData);

			return $this->response->redirect('http://emocionpopular.com.do/conecta');

		}

		catch(\Exception $e)

		{

			$this->flash->error($e->getMessage());

			return;

		}

    }


    public function adminAction()

    {

		if ($this->session->has("userData")) {

           return $this->response->redirect('http://emocionpopular.com.do/admin');

        } 

		

		if (!$this->request->isPost() )

			return ;

			

		if (!$this->request->isPost() && !$this->security->checkToken()) {

            return;

        } 



		if($this->request->getPost('new')) 

			return $this->response->redirect('http://emocionpopular.com.do/registro');



		$username = $this->request->getPost('email');

		$password = $this->request->getPost('password');

		$remember = 1;



		//Ok let validate user password

		$validation = new Validation();

		$validation->add('email', new PresenceOf(['message' => 'No debes dejar el email vacio']));

		$validation->add('password', new PresenceOf(['message' => 'La contraseña no puede estar en blanco']));



		//validate this form for password

		$messages = $validation->validate($_POST);

		if (count($messages))

		{

			foreach ($messages as $message)

			{

				$this->flash->error($message);

			}

			return;

		}

		

		 //login the user

		try

		{

			$users = new Users();

			$userData =  $users->login($username, $password, $remember);

			$this->session->set("userData", $userData);

			return $this->response->redirect('http://emocionpopular.com.do/admin');

		}

		catch(\Exception $e)

		{

			$this->flash->error($e->getMessage());

			return;

		}

    }

	

	public function recuperarAction()

	{

		if ($this->session->has("userData")) {

           return $this->response->redirect();

        } 



		if (!$this->request->isPost() ) {

		

            return;

        }

         

		if($this->request->getPost('new')) 

			return $this->response->redirect('http://emocionpopular.com.do/registro');

			

		$email = $this->request->getPost("email", "email");

		$checkemail = Users::findFirst([

			"email = :email:",

			"bind" => [

				"email" => $email

		]]);

		

		if($checkemail){

			$checkemail->code = md5($this->security->hash($email));

			$checkemail->save(); 

			$this->flash->success("Hemos enviado una solicitud de recuperacion de contraseña, verifica tu correo para validar tu identidad.");

			$this->mail->send($email, "Recuperar Contraseña", "recuperar", ['code' => $checkemail->code]);

			//mail('max@mctekk.com', 'My Subject', $message);

		

           return;

		}else{

			$this->flash->error('Este correo no se encuentra registrado, intenta con otro o Registrate <a href="/registro">aquí </a>');

         return;

		}

		

	}

	

	public function validatecodeAction($code)

	{

		if ($this->session->has("userData")) {

           return $this->response->redirect();

        } 

		$checkemail = Users::findFirst([

			"code = :code:",

			"bind" => [

				"code" => $code

		]]);

		if(!$checkemail){

			$this->flash->error("Ups! el codigo recibido no es valido o ha caducado.");

			return $this->response->redirect("https://emocionpopular.com.do/login");

           /*  return $this->dispatcher->forward(array(

                "controller" => "login",

                "action" => "index"

            )); */

		}

		if (!$this->request->isPost()) {

            return;

        } 

		

		if ($this->request->getPost("password") != $this->request->getPost("password2")) {

			$this->flash->error("Las contraseñas presentan problemas.");

            return $this->dispatcher->forward(array(

                "controller" => "login",

                "action" => "validatecode"

            ));

        }

		

		$checkemail->password = $this->security->hash($this->request->getPost("password"));

		$checkemail->code = "";

		$checkemail->save();

		$this->flash->success("La contraseña fue cambiada, trata de acceder ahora :).");

		 return $this->response->redirect("https://emocionpopular.com.do/login");

		/* return $this->dispatcher->forward(array(

                "controller" => "login",

                "action" => "index"

            )); */

	}

	

	public function facebookAction()

	{

		$user = $this->facebook->fbLogin();

		if($user)

		{

			$checkemail = Users::findFirst([

				"email = :email:",

				"bind" => [

					"email" => $user['email']

			]]);

			if(!$checkemail)

			{

				$this->session->set("facebook", $user);

				$this->flash->success("Completa los campos que faltan.");

				return $this->dispatcher->forward(array(

						"controller" => "registro",

						"action" => "index"

					));

			}

			

			$this->session->set("userData", $checkemail);

			return $this->response->redirect();

		}

	}



	public function logoutAction()
	{
		$this->session->destroy();
		return $this->response->redirect();
	}

	public function recuperadoAction()
	{
		return ;
	}

}

