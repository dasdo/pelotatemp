<?php

use Phalcon\Mvc\Model\Criteria,
    Phalcon\Paginator\Adapter\Model as Paginator;

class SolucionesController extends ControllerBase
{
	 /**
     * Index action
     */
    public function indexAction()
    {
    	$twitter = new Pelota\Tweet();
    	$this->view->setVar('tweet', $twitter->getLastTweet());
    }
}