<?php

use Phalcon\Mvc\Model\Criteria,
    Phalcon\Paginator\Adapter\Model as Paginator;

class BateoController extends ControllerBase
{
	 /**
     * Index action
     */
    public function indexAction()
    {
	if($this->request->isPost() && !$this->request->getPost('search', 'string'))
	{
		 return $this->dispatcher->forward(array(
                "controller" => "bateo",
                "action" => "index"
            ));
		
	}
		$this->view->best = Users::find(['conditions' => 'total_bateo > 0', 'limit' => ['number' => 2, 'offset' => 0], "order" => "total_bateo desc"]);
		$page = $this->request->get('page','int') <= 1 ? 2 : $this->request->get('page','int') + 10;
		$nextPage = $this->request->get('page','int') == 0 ? 2 : $this->request->get('page','int') + 1;
		$prevPage = $this->request->get('page','int') > 0 &&  $this->request->get('page','int') != 1 ? $this->request->get('page','int') - 1 : null;

		$this->view->histories = Users::find(['conditions' => 'total_bateo > 0' , 'limit' => ['number' => 10, 'offset' => $page], "order" => "total_bateo desc"]);

		$search = strtolower($this->request->getPost("search", "string"));
		
		$linkLeft = is_null($prevPage) ? '#' : '/bateo?page='.$prevPage.'#page';
		$linkRight = is_null($nextPage) ? '#' : '/bateo?page='.$nextPage.'#page';

	if ($this->request->isPost()){

		if($search)
		{
			//LOWER(`Value`)
			$users = Users::find([
				'conditions' => "LOWER(email) like ?1 OR LOWER(nombre) like ?2",
				'bind' => [1 => "%{$search}%", 2 => "%{$search}%"]
			]);

			$this->view->users = true;
			$this->view->histories = $users;
		}
	 	}
	 	$twitter = new \Pelota\Tweet();
        $this->view->setVar('tweet', $twitter->getLastTweet());
        $this->view->setVar('linkLeft', $linkLeft);
        $this->view->setVar('linkRight', $linkRight);

		
    }
}
