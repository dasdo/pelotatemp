<?php


class Juegos extends \Phalcon\Mvc\Model
{
	public $id;
	public $subject;
	public $start;
	public $end;
	public $hour;
	public $all_day_event;
	public $description;
	public $location;
	public $private;
	
	public static function migrateCSV(){
		$csv = array_map('str_getcsv', file('/home/dasdo/lab/pelota.csv'));
		unset($csv[0]);
		foreach($csv as $data)
		{
			$game = new Juegos();
			$game->subject = $data[0];
			$game->start = date("Y-m-d", strtotime($data[1]));			
			$game->hour = $data[2];
			$game->end =  date("Y-m-d", strtotime($data[3]));
			$game->all_day_event = $data[5];
			$game->description = $data[6];
			$game->location = $data[7];
			$game->private = $data[8];
			$game->save();
		}
	}
	
	public static function getToday()
	{
		$date = date("Y-m-d");
		// $date = "2014-11-14";
		
		return Juegos::find([
			"start >= :start:",
			"limit" => 28,
			'order' => 'start asc',
			"bind" => [
				"start" => $date
			]
		]);
	}
	
	public static function getInfoGame($subject, $date)
	{
		return Juegos::find([
			"subject = '{$subject}' AND start = '{$date}'"
		])->toArray();
	}
	
}
