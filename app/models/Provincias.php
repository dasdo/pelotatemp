<?php


class Provincias extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id_provincia;
     
    /**
     *
     * @var string
     */
    public $name;
     
    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'id_provincia' => 'id_provincia', 
            'name' => 'name'
        );
    }

}
