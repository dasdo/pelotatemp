<?php


class HistorialBateo extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $historial_id;
     
    /**
     *
     * @var integer
     */
    public $user_id;
     
    /**
     *
     * @var integer
     */
    public $bateo;
     
    /**
     *
     * @var string
     */
    public $created_date;
     
    /**
     * Independent Column Mapping.
     */
    public function columnMap() {
        return array(
            'historial_id' => 'historial_id', 
            'user_id' => 'user_id', 
            'bateo' => 'bateo', 
            'created_date' => 'created_date'
        );
    }
	
	public static function getBest()
	{
		return HistorialBateo::find(array(
				"order" => "bateo DESC",
				"group" => "user_id",
				 "limit" => ['number' => 2 , 'offset' => 0]
			));
	}
	
	public function initialize()
    {
        $this->hasOne("user_id", "Users", "user_id");
    }

}
