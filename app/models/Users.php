<?php


class Users extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $user_id;
     
    /**
     *
     * @var string
     */
    public $nombre;
     
    /**
     *
     * @var string
     */
    public $cedula;
     
    /**
     *
     * @var integer
     */
    public $id_provincia;
     
    /**
     *
     * @var integer
     */
    public $equipo_id;
     
    /**
     *
     * @var string
     */
    public $email;
     
    /**
     *
     * @var string
     */
    public $telefono;
     
    /**
     *
     * @var string
     */
    public $password;
     
    /**
     *
     * @var string
     */
    public $created_date;
     
    /**
     *
     * @var integer
     */
    public $is_active;
	
    public $code;
     
    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Phalcon\Mvc\Model\Validator\Email(
                array(
                    "field"    => "email",
                    "required" => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }
	
	public function initialize()
    {
        $this->hasOne("id_provincia", "Provincias", "id_provincia");
        $this->hasOne("equipo_id", "Equipos", "equipo_id");
		$this->belongsTo("user_id", "HistorialBateo", "user_id");
    }
	
	public function findFirstByEmail($email)
	{
		return Users::findFirst("email='{$email}'");
	}
	
	public function login($username, $password, $autologin = 1)
    {
        //trim username
        $username = ltrim(trim($username));
        $password = ltrim(trim($password));

        //first we find the user
        if ($userInfo = $this->findFirstByEmail($username))
        {
            //will only work with php.5.5 new password api 
            if (password_verify($password, trim($userInfo->password)) && $userInfo->is_active)
            {
                $autologin = (isset($autologin)) ? TRUE : 0;
                if ($userInfo)
                {
                    //login correcto pasa al sistema
                    return $userInfo;
                }
                else
                    throw new \Exception(_("Couldn't start session : login", __LINE__, __FILE__));
            } // Only store a failed login attempt for an active user - inactive users can't login even with a correct password
            elseif ($userInfo->is_active)
                throw new \Exception(_('Contraseña Incorrecta. Por favor intente nuevamente.'));
            else
                throw new \Exception(_('User is not activated'));
        }
        else
            throw new \Exception(_('Este correo no se encuentra registrado, intenta con otro o Registrate <a href="/registro">aquí </a>'));
    }
	
	public function getPosition()
	{
		$history = Users::find(array(
				"order" => "total_bateo DESC"
			));
		foreach($history as $k => $hits)
			if($hits->user_id == $this->user_id)
				return $k + 1;
	}
}
