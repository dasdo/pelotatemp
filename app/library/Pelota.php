<?php namespace Pelota;
	use Phalcon\Mvc\User\Component;
class Pelota extends Component
{
	public static $equipos = [
		"AC" => "AGUILAS CIBAEÑAS",
		"E" => "ESCOGIDO",
		"L" => "TIGRES DEL LICEY",
		"TE" => "TOROS DEL ESTE",
		"EO" => "ESTRELLAS ORIENTALES",
		"GC" => "GIGANTES DEL CIBAO"
	];

	public static $dias = array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sádo");
	public static $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
 
	public static function getNextGame()
	{
		setlocale(LC_ALL,"es_ES");
		$games = \Juegos::getToday();
		$html = '<div class="row"><div class="col-md-3">';
		foreach($games as $k => $game)
		{
			//$date = date("d F Y", strtotime($game->start));
			$date = date("d", strtotime($game->start))." ".self::$meses[date('n',  strtotime($game->start))-1]." ".date("Y", strtotime($game->start));
			$gamers = explode("-", $game->subject);
			$html  .= "	<div class='game'>
							<div class='info'>
								<h3 class='date'>{$date}</h3>
								<span class='place'>@{$game->location}</span>
								<span class='time'>{$game->hour}</span>
								<div class='clear-float'></div>
							</div>
							<div class='vs'>
								<div class='team local'><img src='/images/teams/".Pelota::getImage($gamers[0])."'></div>
								<b class='diamond'>VS</b>
								<div class='team guest'><img src='/images/teams/".Pelota::getImage($gamers[1])."'></div>
							</div>
						</div>";
		}
		$html .="</div></div>";
		return $html;
	}
	
	public static function getGameResult()
	{
		$cache = \Phalcon\DI::getDefault()->getCache();
		$key = 'pelota2014_resultssssss'.date("YmdH");
		if(!$results = $cache->get($key))
		{
			$listResults = function($days = "-1 days") { 
			$date = date("Ymd", strtotime($days, time()));
			$searchDate = date("Y-m-d", strtotime($days, time()));
			
			$dom = new \DOMDocument();
	 
			$html = $dom->loadHTMLFile("http://foxsportslatam.stats.com/DL/scoreboard.asp?day={$date}");
			 
			$dom->preserveWhiteSpace = false;
			 
			$tables = $dom->getElementsByTagName('table');

			for($i=2; $tables->length > $i; $i++)
				$results[] = Pelota::getTable($tables->item($i));
			$results =  Pelota::formatTable($results, $searchDate);

			return $results;
			};
		
			$results = $listResults();
			if(count($results == 0)) $listResults('-2 days');
			$cache->save($key, $results, 86400);
		}
		foreach($results as $game)
		{
			$winA = (int) $game['resultadoa'] > (int) $game['resultadob'] ? 'win' : null;
			$winB = is_null($winA) ? 'win' : null;
			$game['date'] = date('d/m', strtotime($game['date']));
			$html .="
					<table class='column'>
						<tr class='header'>
							<td colspan='2'>@{$game['estadio']} {$game['date']}</td>
							<td class='points' style='color: #00498f;'>R</td>
						</tr>
						<tr>
							<td class='logo'><img src='images/teams/".Pelota::getImage($game['equipob'])."' alt=''/></td>
							<td>".Pelota::$equipos[$game['equipob']]."</td>
							<td class='points {$winB}'>{$game['resultadob']}</td>
						</tr>
							<tr>
							<td class='logo' style='width: 2%;'><img src='images/teams/".Pelota::getImage($game['equipoa'])."' alt=''/></td>
							<td style='width: 80%;'>".Pelota::$equipos[$game['equipoa']]."</td>
							<td class='points {$winA}' style='width: 20%'>{$game['resultadoa']}</td>
						</tr>
					</table>
				";
		}

		return $html;
	}
	
	
	/**
	* Deveulve un arreglo de los standing de los equipos de pelota
	* @return Array
	*/
	public static function getStanding()
	{
		$cache = \Phalcon\DI::getDefault()->getCache();
		$key = 'pelota2014_standing'.date('Ymd');

		if(!$table = $cache->get($key))
		{
			$dom = new \DOMDocument();
	 
			$html = $dom->loadHTMLFile("http://lidom.digisport.com.do/Estadisticas/Standings/Standings");
			 
			$dom->preserveWhiteSpace = false;
			 
			$tables = $dom->getElementsByTagName('table');
			  
			//get all rows from the table
			$rows = $tables->item(0)->getElementsByTagName('tr');
			// get each column by tag name
			$cols = $rows->item(0)->getElementsByTagName('th');
			$row_headers = NULL;
			foreach ($cols as $node) {
			    //print $node->nodeValue."\n";
			    $row_headers[] = $node->nodeValue;
			}
			 
			$table = array();
			//get all rows from the table
			$rows = $tables->item(0)->getElementsByTagName('tr');

			foreach ($rows as $row)
			{
			    // get each column by tag name
			    $cols = $row->getElementsByTagName('td');
			    $row = array();
			    $i=0;
			    foreach ($cols as $node) 
			    {
			        # code...
			        //print $node->nodeValue."\n";
			        if($row_headers==NULL)
					{
			            $row[] = trim($node->nodeValue);
					}
			        else
					{
					    $row_headers[$i] = str_replace(['2013', '2012'] , '' , $row_headers[$i]);
					    $row_headers[$i] = str_replace(['2014'] , 'Equipo' , $row_headers[$i]);
					    $row_headers[$i] = str_replace(['Serie Regular', 'Round Robin'] , '' , $row_headers[$i]);
					    $row_headers[$i] = str_replace(['Serie Final'] , 'Je' , $row_headers[$i]);
			            $row[trim($row_headers[$i])] = trim($node->nodeValue);
					}

			        $i++;
			    }

			    $table[] = $row;
			}

			$cache->save($key, $table, 86400);
		}

		return $table;
	}
	
	public static function getImage($equipo)
	{
			switch (trim($equipo)) 
			{
				case "AC":
						$logo = 'logo_aguilas.png';
					break;
				case "TE":
						$logo = 'logo_toros.png';
					break;
				case "EO":
						$logo = 'logo_estrellas.png';
					break;					
				case "GC":
						$logo = 'logo_gigantes.png';
					break;
				case "L":
						$logo = 'logo_licey.png';
					break;
				case "E":
						$logo = 'logo_escogido.png';
					break;
			}
		return $logo;
	}
	
	public static function getTable($items)
	{
		//get all rows from the table
		$rows = $items->getElementsByTagName('tr');
		// get each column by tag name
		$cols = $rows->item(0)->getElementsByTagName('th');
		$row_headers = NULL;
		foreach ($cols as $node)
			$row_headers[] = $node->nodeValue;
		$table = array();
		//get all rows from the table
		$rows = $items->getElementsByTagName('tr');

		foreach ($rows as $row)
		{
			// get each column by tag name
			$cols = $row->getElementsByTagName('td');
			$row = array();
			$i=0;
			foreach ($cols as $node) 
			{
				# code...
				//print $node->nodeValue."\n";
				if($row_headers==NULL)
					$row[] = trim($node->nodeValue);
				else
					$row[trim($row_headers[$i])] = trim($node->nodeValue);
				$i++;
			}

			$table[] = $row;
		}
		return $table;
	}
	
	public static function formatTable($tables, $date)
	{
		$data = [];
		foreach($tables as $table)
		{
			$a = Pelota::remplaceName($table[1][0]);
			$b = Pelota::remplaceName($table[2][0]);
			$subject = $b."-".$a;
			$subjectb = $a."-".$b;
			$juego = \Juegos::getInfoGame($subject, $date);
			if(!isset($juego[0]['location'])) $juego = \Juegos::getInfoGame($subjectb, $date);
			$estadio = isset($juego[0]['location']) ? $juego[0]['location'] : "";
			$data[] = [
				'estadio' => $estadio,
				'equipoa' => $a,
				'equipob' => $b,
				'resultadoa' =>  $table[1][1],
				'resultadob' => $table[2][1],
				'date' =>  date("d F Y", strtotime($date))
			];
		}
		return $data;
	}
	
	public static function remplaceName($name)
	{
		switch ($name) 
		{
			case "Cibaenas Aguilas":
					return "AC";
				break;
			case 'Este Toros':
					return "TE";
				break;
			case 'Orientes Estrellas':
					return "EO";
				break;					
			case 'Cibao Gigantes':
					return "GC";
				break;
			case 'Licey Tigres':
					return "L";
				break;
			case 'Escogido Leones':
					return "E";
				break;																				
		}
	}
	
	public static function getMobileDetect()
	{
		return new \Mobile_Detect();
	}
}
?>
