 <?php
require_once '/home/emocionpopularco/pelotatemp/public/mandrill/src/Mandrill.php';
// require_once '/home/dasdo/pelota/public/mandrill/src/Mandrill.php';

class Mail extends Phalcon\Mvc\User\Component
{

	protected $_transport;

	public function getTemplate($name, $params)
	{
		
		$css = file_get_contents("https://emocionpopular.com.do/css/style.css");
				$body = '
<!DOCTYPE HTML>
<html>
<head>
	<title>Emoción Popular</title>
	<meta charset="UTF-8"> 
	<!-- styles -->
	<style>


/*====================================================================================================================

===================================================== TYPOGRAPHY =====================================================

====================================================================================================================*/



@font-face {

    font-family: "OceanSans"; 

    src: url("https://emocionpopular.com.do/fonts/ocean/oceanlt_.eot");

    src: url("https://emocionpopular.com.do/fonts/ocean/oceanlt_.eot") format("embedded-opentype"),

         url("https://emocionpopular.com.do/fonts/ocean/oceanlt_.woff") format("woff"),

         url("https://emocionpopular.com.do/fonts/ocean/oceanlt_.ttf") format("truetype"),

         url("https://emocionpopular.com.do/fonts/ocean/oceanlt_.svg#OceanSansMTLightRoman") format("svg");

}



@font-face { 

    font-family: "OpenSans";

    src: url("https://emocionpopular.com.do/fonts/OpenSans/opensans-regular.eot");

    src: url("https://emocionpopular.com.do/fonts/OpenSans/opensans-regular.eot") format("embedded-opentype"),

         url("https://emocionpopular.com.do/fonts/OpenSans/opensans-regular.woff") format("woff"),

         url("https://emocionpopular.com.do/fonts/OpenSans/opensans-regular.ttf") format("truetype"),

         url("https://emocionpopular.com.do/fonts/OpenSans/opensans-regular.svg#OpenSansRegular") format("svg");

}



@font-face {

    font-family: "OceanSansBold";

    src: url("https://emocionpopular.com.do/fonts/oceanbold/oceansb_.eot");

    src: url("https://emocionpopular.com.do/fonts/oceanbold/oceansb_.eot") format("embedded-opentype"),

         url("https://emocionpopular.com.do/fonts/oceanbold/oceansb_.woff2") format("woff2"),

         url("https://emocionpopular.com.do/fonts/oceanbold/oceansb_.woff") format("woff"),

         url("https://emocionpopular.com.do/fonts/oceanbold/oceansb_.ttf") format("truetype"),

         url("https://emocionpopular.com.do/fonts/oceanbold/oceansb_.svg#OceanSansMTSemiBoldRoman") format("svg");

}



@font-face {

    font-family: "OceanSansExtraBold";

    src: url("https://emocionpopular.com.do/fonts/oceanExtraBold/oceaneb_.eot");

    src: url("https://emocionpopular.com.do/fonts/oceanExtraBold/oceaneb_.eot") format("embedded-opentype"),

         url("https://emocionpopular.com.do/fonts/oceanExtraBold/oceaneb_.woff2") format("woff2"),

         url("https://emocionpopular.com.do/fonts/oceanExtraBold/oceaneb_.woff") format("woff"),

         url("https://emocionpopular.com.do/fonts/oceanExtraBold/oceaneb_.ttf") format("truetype"),

         url("https://emocionpopular.com.do/fonts/oceanExtraBold/oceaneb_.svg#OceanSansMTExtraBoldRoman") format("svg");

}

/*====================================================================================================================

=================================================== GENERAL STYLES ===================================================

====================================================================================================================*/

* { box-sizing: border-box; }

body {

	color: #333333;

	font-family: "OpenSans";

	font-size: 15px;

	line-height: 18px;

	margin: 0;

}

section { margin-bottom: 40px; }

section:last-child { margin-bottom: 0; }


.content, .main-content { margin: 0 auto; width: 940px; }


.main-content { min-height: 500px; }

.main-content.top { margin-top: 65px; }


.clear-float { clear: both; }


.left { float: left; }

.right { float: right; }


/*WRAPPER STYLES*/

#wrapper { overflow: hidden; padding-top: 40px; }

#wrapper.homerun { background: #000 url("https://emocionpopular.com.do/images/background.jpg") no-repeat center top; }

#wrapper.ranking { background: #000 url("https://emocionpopular.com.do/images/ranking_bg.jpg") no-repeat center -8px; }

#wrapper.product { background: #00498f url("https://emocionpopular.com.do/images/product_bg.jpg") repeat-x; }

#wrapper.darkblue { background-color: #001a33; }

#wrapper.game { background: #000 url("https://emocionpopular.com.do/images/backgroundgame.jpg") no-repeat center top; }


/*TEXT STYLES*/

h1, h2, h3 { font-family: "OceanSansBold"; }



/*RIBBORNS*/

.ribborn-title { 

	background: #FFF;

	border: 3px solid #004990;

	color: #00498f;

	display: block;

	font-family: "OceanSansBold";

	font-size: 21px;

	margin: 0 auto -10px;

	padding: 10px 30px;

	position: relative;

	text-align: center;

	text-decoration: none;

	text-transform: uppercase;

	top: -28px;

}


.ribborn-title.yellow { background-color: #ffa800; }


.ribborn-title:before, .ribborn-title:after { 

	content: "";

	height: 42px;

	position: absolute;

	top: -2px;

	width: 25px;

	z-index: 1;

 }

 .ribborn-title:before { background-position: -73px -300px !important; left: -19px; }

 .ribborn-title:after { background-position: -104px -300px !important; right: -18px; } 


.ribborn-title.yellow:before { background-position: -1px -301px !important; left: -19px; }

.ribborn-title.yellow:after { background-position: -36px -301px !important; right: -18px; }


/*SPRITE IMAGES*/ 

.dotted_line, #logo, nav ul li.current:before, nav ul li:hover:before, nav ul li.current:after, 

nav ul li:hover:after, .logo_game, .ribborn-title:before, .ribborn-title:after, .ribborn-title.yellow:before, 

.ribborn-title.yellow:after, .blue_ball, #results:before, #results:after, .twittericon, .favorite, .retweet, .respond,

.tweetbox_triangle, .white_arrowdown, .blue_arrowdown, .diamond, .gold_medal, .silver_medal, .glass, .online, 

#slides .pagination li a, #slides .pagination li.current a 

{ background: url("https://emocionpopular.com.do/images/sprites.png") no-repeat; display: block; }

.dotted_line { 

	background-position: 0 -171px ; 

	background-repeat: repeat-x; 

	height: 4px;

	width: 100%; 

}

.logo_game { 

	background-position: -261px 0;

	height: 127px;

	width: 153px;

}

/*COLUMNS*/

.column { display: inline-block; width: 300px; }

.column:nth-child(2) { margin: 0 16px; }

/*TWITTER BOX*/

.socialblock.fullwidth { background: url("https://emocionpopular.com.do/images/paper.jpg"); height: 180px; }

	.socialblock.fullwidth #w-twitter { 

		margin: 0 auto;

		position: relative; 

		top: -30px;

	}

		.socialblock.fullwidth #w-twitter #tweetbox { border-top: 0; }



	#w-twitter { width: 620px; }

		#tweetbox {

			background-color: #FFF; 

			border-top: 10px solid #00498f;

			box-shadow: 0px 3px 4px #888888;

			margin-bottom: 20px;

			position: relative;

		}

		#tweetbox .tweetbox_triangle {

			background-position: -264px -244px;

			bottom: -27px;

			height: 27px;

			left: 70px;

			position: absolute;

			width: 24px;		

		}

			#tweetbox .wrap_text { padding: 20px; }

				#tweetbox .wrap_text h2 { 

					color: #00498f; 

					font-style: italic; 

					margin: 0 0 8px;

				}
			
				#tweetbox .wrap_text p { color: #333; margin: 0; }

			#tweetbox .footer { 

				color: #A9A9A9;

				font-size: 12px;

				overflow: hidden;

				padding: 10px 20px; 

			}

				#tweetbox .footer .r-amount { float: left; }

					#tweetbox .footer b { color: #666; }

				#tweetbox .footer .options { float: right; }

					#tweetbox .footer .options a { 

						float: left;

						height: 17px; 

						margin-right: 5px;

						text-decoration: none;

						width: 21px; 

					}

					#tweetbox .footer .options a:last-child { margin-right: 0; }

					#tweetbox .footer .options a.respond { background-position: -108px -248px; }

					#tweetbox .footer .options a.retweet { background-position: -141px -248px; }

					#tweetbox .footer .options a.favorite { background-position: -170px -247px; }


		#w-twitter .twittericon { 

			background-position: -207px -240px; 

			float: left;

			height: 36px;

			margin-left: 20px;

			width: 43px;

		}

		#w-twitter span.mention { 

			color: #00498f;

			float: left; 

			font-family: "OceanSans";

			font-size: 21px;

			font-style: italic;

			margin: 10px 0 0 8px;

		}

/*====================================================================================================================

====================================================== HEADER ========================================================

====================================================================================================================*/

header { background-color: #00498f; position: relative; }

	header span.dotted_line { position: absolute; z-index: 1; }

		header span.dotted_line.top { top: 3px; }

		header span.dotted_line.bottom { bottom: 3px; }

	/*LOGOS*/

	header h1#logo { 

		background-position: 0 0; 

		height: 151px;

		margin: 0;

		position: absolute;

		top: -25px;

		width: 248px;

		z-index: 2;

	}

	.popular_logo { float: right; margin-top: 12px; }

	/*NAV BAR*/

	nav { float: left; margin-left: 290px; }

	nav ul { margin: 0; padding: 0; }

		nav ul li { 

			display: block;

			float: left; 

			font-family: "OceanSansBold";

			list-style: none;

			        font-style: italic;

			margin-right: 20px;

			position: relative; 

			text-transform: uppercase;

		}

		nav ul li:hover, nav ul li.current { background: url("https://emocionpopular.com.do/images/nav_hover.png") repeat-x; }

		nav ul li.current:before, nav ul li.current:after, nav ul li:hover:before, nav ul li:hover:after {

			content: "";

			display: block;

			height: 68px;

			position: absolute;

			top: 0;

			width: 14px;

		}

		nav ul li.current:before, nav ul li:hover:before { background-position: -136px -298px ; left: -14px;  }

		nav ul li.current:after, nav ul li:hover:after { background-position: -160px -300px ; right: -14px;  }

		nav ul li a { 

			color: #FFF;

			display: block;

			padding: 24px 10px;

			text-decoration: none;

		}

/*====================================================================================================================

================================================== HOMERUN SECTION ===================================================

====================================================================================================================*/

#videos { height: 336px; width: 540px; }

	#videos object { height: 100%; width: 100%; }

#game_advertisement { color: #FFF; width: 380px; }

	#game_advertisement .banner {

		background: url("https://emocionpopular.com.do/images/base.png") no-repeat;

		border-top: 4px solid #0070b5;

		height: 257px;

		margin: 0 auto;

		padding: 20px;

		position: relative;

		text-align: center;

		width: 288px;

	}

		#game_advertisement .banner .logo_game { 

			left: 70px;

			position: absolute; 

			top: -50px; 

		}



		#games_advertisement .banner h3 { 

			color: #00498f; 

			font-size: 21.7px;

			margin: 70px 0 0; 

		}

		/*Safari Hack*/

		@media screen and (-webkit-min-device-pixel-ratio:0) { 

			#games_advertisement .banner h3 a { font-size: 21.7px; }

		    /* Safari only override */

		    ::i-block-chrome,#games_advertisement .banner h3 a { font-size: 21.7px; }  

		}

		#game_advertisement .banner .btn-game { font-family: "OceanSansExtraBold"; top: 0; }

	#game_advertisement .blue_ball { 

		background-position: 0 -247px; 

		height: 22px;

		margin: 10px auto; 

		width: 51px;

	}

	#game_advertisement h2 { 

		font-family: "OceanSans";

		font-size: 27px; 

		font-style: italic; 

		line-height: 30px; 

	}

#games { position: relative; }

	/*PROXIMOS JUEGOS*/

	aside { 

		left: 0;

		position: absolute;

		top: 0;

	}
		aside h3.ribborn-title { 

			margin-bottom: -44px; 

			width: 254px; 

			z-index: 1;

		}	

		#next_game { 

			background-color: #fff;

			border-top: 10px solid #00498f;

			box-shadow: 1px 2px 4px #868686;

			height: 500px;

			overflow: hidden;

			width: 300px;    

		}

			#next_game .game { 

				border-bottom: 1px solid #666; 

				border-top: 1px solid #666;

				margin-bottom: -1px; 

				text-transform: uppercase;

			}

			#next_game .game:first-child { margin-top: 20px; }



				#next_game .game .info { 

					border-bottom: 1px solid #666; 

					position: relative;

				}

				#next_game .game .info:after { 

					border-color: #666666 transparent transparent transparent;

					border-style: solid;

					border-width: 8px 8px 0 8px;

					bottom: -8px;

					content: "";

					left: 47%;

					position: absolute;

				}
					#next_game .game .info .date { 

						background-color: #e2f4ff;

						color: #00498f;

						font-family: "OpenSans";

						font-size: 20px;

						margin: 0; 

						padding: 15px 30px; 

						text-align: center;

					}

					#next_game .game .info span { font-size: 18px; display: block; }



					#next_game .game .info span.place {

						color: #666; 

						float: left;

						padding: 8px 0 8px 30px; 

						width: 64%; 

					}



					#next_game .game .info span.time { 

						background-color: #666;

						color: #FFF;

						float: right;

						padding: 8px 15px;

						width: 33%;

					}



				#next_game .game .vs { overflow: hidden; padding: 15px 30px; }

					#next_game .game .vs .team { width: 30%; }

					#next_game .game .vs .team.local { float: left; }

					#next_game .game .vs .team.guest { float: right; }

						#next_game .game .vs .team img { width: 100%; }



					#next_game .game .vs .diamond {

						background-position: -436px -238px ;

						color: #FFF;

						display: block; 

						float: left;

						height: 43px;

						line-height: 40px;

						margin: 10px 26px 0;

						text-align: center; 

						width: 44px;

					}



		/*aside a.scrolldown {  }*/



	/*TABLA DE POSICIONES*/

	#team_position {

		background-color: #00498f;

		color: #FFF;

		margin-bottom: 50px;

		padding: 0 15px 15px;

		text-transform: uppercase;

		width: 620px;

	}



		#team_position h3.ribborn-title { width: 290px; }



		#team_position table { 

			border-collapse: collapse; 

			font-size: 12px;

			width: 100%; 

		}



			#team_position table tr:nth-child(2n) { background-color: #0070b5 }



				#team_position table th { 

					border-bottom: 1px solid #00a2e5; 

					border-right: 1px solid #00a2e5; 

					padding: 5px;

				}



				#team_position table td { 

					border-right: 1px solid #00a2e5; 

					padding: 5px;

					text-align: center; 

					vertical-align:middle;

				}



				#team_position table .logo { border-right: 0; }



				#team_position table td.align-left { text-align: left; }



				#team_position table th:last-child, #team_position table td:last-child { border-right: 0; }



					#team_position table td.logo img { width: 28px; }



	#games .socialblock { 

		background: url("https://emocionpopular.com.do/images/paper.jpg"); 

		clear: both;

		height: 256px;

		margin-left: -35px;	

		width: 107%;

	}



		#games .socialblock #w-twitter { float: right; margin: -20px 30px 0 0; }



		#games .socialblock .blue_arrowdown {

			background-position: -297px -246px;

			bottom: 30px;

			height: 21px;

			left: 48%;

			position: absolute;

			width: 36px;				

		}



/*RESULTADOS*/

#results { 

	background-color: #004990; 

	padding: 15px 0; 

	position: relative;

}



#results:before { 

	background-position: -1px -191px ;

	background-repeat: repeat-x;

	content: "";

	height: 12px;

	position: absolute;

	top: -10px;

	width: 100% 

}



#results:after { 

	background-position: -1px -213px ;

	background-repeat: repeat-x;

	content: "";

	height: 18px;

	position: absolute;

	bottom: -16px;

	width: 100% 

}



	#results h1 { 

		display: block;

		margin: 0 0 20px; 

		text-align: center;

	}



	#results table { 

		background-color: #FFF; 

		color: #666; 

		font-size: 19px;

		text-transform: uppercase; 

	}



		#results table tr { border-bottom: 1px solid #004990; }

		#results table tr.header td { height: auto; padding: 10px 15px; }



			#results table td { 

				border-right: 1px solid #004990; 

				height: 50px;

				padding: 0 15px; 

			}



			#results table td:last-child, #results table .logo { border-right: 0; }

			#results table td.logo { padding-right: 0; }

			#results table td.points { font-weight: bold; text-align: center; }

			#results table td.win { background-color: #e2f4ff; }



				#results table .logo img { width: 40px; }



	#results .white_arrowdown {

		background-position: -346px -246px;

		margin: 20px auto 10px;

		height: 21px;

		width: 36px;	

		transform: rotate(180deg);			

	}



/*====================================================================================================================

================================================== HOMERUN SECTION ===================================================

====================================================================================================================*/



#searchbox { 

	background: url("https://emocionpopular.com.do/images/paper_texture.png") repeat-x; 

	box-shadow: 0 3px 6px #8C8C8C inset;

	height: 238px; 

}



	#searchbox #search { 

		float: right; 

		margin-top: 10px; 

		position: relative;

	}



		#searchbox #search input { border: 1px solid #00498e; padding: 5px; }



		#searchbox #search .glass{

			background-position: -68px -249px ;

			height: 21px;

			position: absolute;

			right: 8px;

			top: 5px;

			width: 21px;

		}



	#searchbox h1 { 

		clear: both;

		margin: 0; 

		position: relative;

		text-align: center; 

		top: 30px;

	}



.blueline { 

	background: #004990; 

	height: 10px; 

	margin: -130px 0 5px;

	width: 100%;

}



#player-position { 

	background-color: #004990; 

	color: #FFF; 

	padding: 20px 0;

}



	#player-position table { 

		border-collapse: collapse;

		font-family: "OceanSansBold";

		margin: 0 auto;

		text-align: center;

		width: 780px;

	}



		#player-position table tr { border-bottom: 1px solid #FFF; }

		#player-position table tr:last-child { border-bottom: 0; }



			#player-position table th { 

				font-size: 21px;

				font-weight: normal;

				padding: 0 20px 10px; 

				text-transform: uppercase;

			}



			#player-position table .name { text-align: left; }

			#player-position table th, #player-position table td { border-right: 1px solid #FFF; padding: 10px 15px; }

			#player-position table th:last-child, #player-position table td:last-child { border-right: 0; }



			#player-position table td.points { font-size: 60px; }

			#player-position table td.name { font-size: 24px; font-style: italic; }

			#player-position table td.province { text-transform: uppercase; }

				

				#player-position table td.medal span { 

					height: 83px;

					margin: 0 auto;

					width: 57px;

				}



				#player-position table td.medal .gold_medal { background-position: 0 -384px; }

				#player-position table td.medal .silver_medal{ background-position: -62px -384px ; }



				#player-position table td.logo img { width: 60px; }



	#player-position .socialblock { 

		background-color: #FFF; 

		margin-top: 20px;

		overflow: hidden;

		padding: 20px 0; 

	}



		#player-position .socialblock #w-twitter { margin: 0 auto; }



	#allplayers h3.ribborn-title { 

		margin: 20px auto; 

		top: 0;

		width: 280px; 

	}



		#allplayers table tr:nth-child(2n) { background-color: #0070b5; }



			#allplayers table td.points { font-size: 28px; }

			#allplayers table td.name { font-size: 18px; }



				#allplayers table td.logo img { width: 40px; }



/*====================================================================================================================

==================================================== GAME FORMS ======================================================

====================================================================================================================*/



.formbox { 

	background-color: #00498e; 

	margin: -35px auto 0;

	position: relative;

	width: 876px;

}

	

	.formbox .logo_game { 

		height: 108px;

		left: 42%;

		position: absolute; 

		top: 20px;

	}



	.formbox .content-form { 

		background: url("https://emocionpopular.com.do/images/paper.jpg"); 

		margin-top: 10px; 

		padding-bottom: 10px;

	}



		.formbox .content-form h3.ribborn-title { margin: 0 auto; width: 200px; }



		.formbox .content-form h4 {

			color: #00498e;

			line-height: 28px;

			font-family: "OceanSans";

			font-size: 25px; 

			font-style: italic;

			margin: 0 auto 40px; 

			text-align: center;

			width: 84%;

		}



		.formbox .content-form form { height: 280px; margin: 0 auto; }

		.formbox .content-form form#login { width: 52%;  }

		.formbox .content-form form#signup { width: 86%; }



			.formbox .content-form form input[type="text"], .formbox .content-form form input[type="password"], 

			.formbox .content-form select { 

				border: 1px solid #00498e;

				border-left-width: 8px;

				margin-bottom: 15px;

				padding: 8px 20px;

				width: 100%;

			}



			.formbox .content-form form .row { margin-bottom: 10px; overflow: hidden; }

			.formbox .content-form form .row.short { margin: 0 auto; width: 70%; }



				.formbox .content-form form .col { float: left; width: 31.52%; }		

				.formbox .content-form form .col:nth-child(2n) { margin: 0 20px; }



				.formbox .content-form form .row.short .col { width: 48%; }	

				.formbox .content-form form .row.short .col:nth-child(2n) { margin: 0 0 0 20px; }



			.formbox .content-form form a.recovery { 

				color: #00498e; 

				display: block;

				float: right; 

				font-size: 18px;

				margin-bottom: 20px;

				text-decoration: none;

			}



			.formbox .content-form form .buttons { clear: both; overflow: hidden; }

			.formbox .content-form form#signup .buttons { margin: 30px auto 0; width: 250px; }



				.formbox .content-form form .buttons input[type="submit"] { 

					border: 0;

					color: #FFF;

					font-size: 16px;

					padding: 12px 30px;

					text-transform: uppercase;

				 }

				

				.formbox .content-form form .buttons .btn-orange { background: url("https://emocionpopular.com.do/images/orange_G.jpg") repeat-x; float: left; }

				.formbox .content-form form .buttons .btn-blue { background: url("https://emocionpopular.com.do/images/blue_G.jpg") repeat-x; float: right; }



/*====================================================================================================================

================================================= PRACTICE SECTION ===================================================

====================================================================================================================*/

#practice { 

	margin: -35px auto -3px; 

	text-align: center;

	width: 876px; 

}



#status { 

	background-color: #004990;

	color: #FFF;

	text-align: right;

	text-transform: uppercase;

}



	#status .content { 

		background-color: #003262; 

		padding: 10px 20px 15px; 

		width: 876px; 

	}



		#status span { display: inline-block; }



		#status .online {

			background-position: -400px -247px;

			height: 19px;

			position: relative;

			top: 3px;

			width: 20px;

		}



/*====================================================================================================================

=================================================== PRODUCT SECTION ==================================================

====================================================================================================================*/



#slides { position: relative; }



	#slides .slides_container {

		border: 3px solid #FFF; 

		border-radius: 5px;

		display:none;

		height: 385px;

		margin: 0 auto;

		width: 780px;

	}



		#slides .slides_container div { 

			display: block; 

			height: 385px;

			width: 780px;

		}



	#slides .next, #slides .prev { display: none; }



	#slides .pagination { 

		bottom: 10px;

		left: 38%;

		list-style: none;

		margin: 0;

		padding: 0;

		position: absolute;

		width: 400px;

		z-index: 99;

	}



		#slides .pagination li { display: inline-block; margin-right: 5px; }

		

			#slides .pagination li a { 

				background-position: -534px -243px;

				color: #00498f; 

				font-family: "OceanSansBold";

				font-size: 16px;

				height: 32px;

				line-height: 32px;

				display: block;

				text-align: center;

				text-decoration: none; 

				width: 31px;

			}



			#slides .pagination li.current a { color: #FFF; background-position: -493px -243px ; }



#product .thumbnails { 

	margin: 20px auto 60px; 

	overflow: hidden;

	width: 780px; 

}



	#product .thumbnails ul { margin: 0 0 0 -10px; padding: 0; }



		#product .thumbnails ul li { 

			cursor: pointer;

			float: left;

			height: 74px;

			list-style: none;

			margin: 0 0 10px 10px; 

			width: 121px;

		}



			#product .thumbnails ul li img { height: 100%; width: 100%; }



/*====================================================================================================================

=================================================== FOOTER SECTION ===================================================

====================================================================================================================*/



footer { background: url("https://emocionpopular.com.do/images/sand_texture.jpg"); clear:both; }



	footer .main { background: url("https://emocionpopular.com.do/images/footer_img.png") no-repeat center top; padding: 40px 0; height:1%; overflow:hidden; }



		footer .column {

			background-image: url("https://emocionpopular.com.do/images/transparent.png");

			box-shadow: 0 -4px 0 #FFF inset;

			margin-top: 40px;

			height: 470px;

            float:left;

		}



		footer .column:nth-child(2) { min-height: 500px; margin-top:40px; }



			footer .column h3.ribborn-title { width: 80%; }



	footer #copy { 

		background-color: #004990; 

		color: #FFF;

		font-family: "OceanSans";

		font-size: 12px;

		font-style: italic;

		padding: 20px 0; 

		text-align: center;

	}



.arow-blue{

	position: absolute;

	bottom: -38px;

}


a.pagination {

display: block;

height: 20px;

position: absolute;

top: 9px;

width: 20px;

z-index: 2;

}


a.pagination.btn_prev { left: -22px; }

a.pagination.btn_next { right: -22px; }



.fb-like-box { color: white; }

.errorMessage{ text-align:center; } 

.ScrollStyle
{
    max-height: 450px;
    overflow-y: scroll;
}
		</style>
</head>
	<body>

			<div id="wrapper" class="game"  >
	
		<!-- CONTENEDOR PRINCIPAL -->
		<div class="main-content top">
			<div class="formbox" style="maring:10px; padding: 10px;">
			<div style="margin:30px; text-align:center;"><img src=\'https://emocionpopular.com.do/images/ranking-title-white.png\' /></div>
						
			<div class="content-form">
				<h3 class="ribborn-title" style="width:580px; ">¿Solicitaste recuperar tu contraseña?</h3>
				<h4>Para volver a vivir la #EmociónMásPopular haz clic en recuperar</h4>
			<div class="buttons" style="text-align:center;">
			<a href="http://emocionpopular.com.do/login/validatecode/'.$params['code'].'">
				<button type="button" class="btn-orange" style="border: 0; color: #FFF; font-size: 16px; padding: 12px 30px; text-transform: uppercase; background: url(\'http://emocionpopular.com.do/images/orange_G.jpg\') repeat-x;">!Recuperar!</button>
			</a>	
			</div><br />
			
			</div>
			</div>
		</div>

		</body>
		</html>
';
			
		return $body;
	}
	public function send($to, $subject, $name, $params)
	{
		$template = $this->getTemplate($name, $params);
		$mandrill = new \Mandrill('s4EWu7X39FWZTN-Ln6xVpQ');
		$message = [
				'html' => $template,
				'subject' => $subject,
				'from_email' => 'noreply@emocionpopular.com.do',
				'from_name' => 'Popular',
				'to' => [
					[
						'email' => $to
					]
				],
				 'track_opens' => false,
				 'track_clicks' => false,
				 'auto_html'=> true,
				 'inline_css' => true,
			];
		return $mandrill->messages->send($message);
	}  
}
